//WEAPON CASE SKINS//
Operation Breakout Weapon Case Skins
M4A1-S | Cyrex <> 7
P90 | Asiimov <> 7
Desert Eagle | Conspiracy <> 6
Five-SeveN | Fowl Play <> 6
Glock-18 | Water Elemental <> 6
Nova | Koi <> 5
P250 | Supernova <> 5
PP-Bizon | Osiris <> 5
CZ75-Auto | Tigris <> 5
MP7 | Urban Hazard <> 4
Negev | Desert-Strike <> 4
SSG 08 | Abyss <> 4
UMP-45 | Labyrinth <> 4
P2000 | Ivory <> 4

//COLLECTION SKINS//
The Alpha Collection Skins
SCAR-20 | Emerald <> 5
FAMAS | Spitfire <> 5
AUG | Anodized Navy <> 4
PP-Bizon | Rust Coat <> 4
MAG-7 | Hazard <> 4
P250 | Facets <> 3
Sawed-Off | Mosaico <> 3
Negev | Palm <> 3
SSG 08 | Mayan Dreams <> 3
Glock-18 | Sand Dune <> 3
MP7 | Groundwater <> 2
XM1014 | Jungle <> 2
Five-SeveN | Anodized Gunmetal <> 2
MP9 | Dry Season <> 2
Tec-9 | Tornado <> 2
M249 | Jungle DDPAT <> 2

The Assault Collection Skins
Glock-18 | Fade <> 5
MP9 | Bulldozer <> 5
AUG | Hot Rod <> 4
Negev | Anodized Navy <> 4
Five-SeveN | Candy Apple <> 3
UMP-45 | Caramel <> 2
SG 553 | Tornado <> 2

The Aztec Collection Skins
Tec-9 | Ossified <> 4
M4A4 | Jungle Tiger <> 3
AK-47 | Jungle Spray <> 3
SSG 08 | Lichen Dashed <> 2
Five-SeveN | Jungle <> 2
Nova | Forest Leaves <> 2

The Baggage Collection Skins
AK-47 | Jet Set <> 6
Desert Eagle | Pilot <> 5
AK-47 | First Class <> 5
Sawed-Off | First Class <> 4
USP-S | Business Class <> 4
XM1014 | Red Leather <> 4
P90 | Leather <> 3
MAC-10 | Commuter <> 3
P2000 | Coach Class <> 3
SG 553 | Traveler <> 3
G3SG1 | Contractor <> 2
MP7 | Olive Plaid <> 2
CZ75-Auto | Green Plaid <> 2
MP9 | Green Plaid <> 2
SSG 08 | Sand Dune <> 2

The Bank Collection Skins
P250 | Franklin <> 6
AK-47 | Emerald Pinstripe <> 5
CZ75-Auto | Tuxedo <> 4
Desert Eagle | Meteorite <> 4
Galil AR | Tuxedo <> 4
G3SG1 | Green Apple <> 3
Glock-18 | Death Rattle <> 3
MAC-10 | Silver <> 3
Nova | Caged Steel <> 3
UMP-45 | Carbon Fiber <> 3
MP7 | Forest DDPAT <> 2
Negev | Army Sheen <> 2
Sawed-Off | Forest DDPAT <> 2
SG 553 | Army Sheen <> 2
Tec-9 | Urban DDPAT <> 2

The Cache Collection Skins
Galil AR | Cerberus <> 5
FAMAS | Styx <> 5
Tec-9 | Toxic <> 4
Glock-18 | Reactor <> 4
XM1014 | Bone Machine <> 4
MAC-10 | Nuclear Garden <> 4
MP9 | Setting Sun <> 4
AUG | Radiation Hazard <> 3
PP-Bizon | Chemical Green <> 3
Negev | Nuclear Waste <> 3
P250 | Contamination <> 3
Five-SeveN | Hot Shot <> 3
SG 553 | Fallout Warning <> 3

The Chop Chop Collection Skins
Glock-18 | Twilight Galaxy <> 6
M4A1-S | Hot Rod <> 6
SG 553 | Bulldozer <> 5
Dual Berettas | Duelist <> 5
MAC-10 | Fade <> 4
P250 | Whiteout <> 4
MP7 | Full Stop <> 4
Five-SeveN | Nitro <> 4
CZ75-Auto | Emerald <> 4
Desert Eagle | Night <> 3
Galil AR | Urban Rubble <> 3
USP-S | Para Green <> 3
SCAR-20 | Army Sheen <> 2
CZ75-Auto | Army Sheen <> 2
M249 | Impact Drill <> 2
MAG-7 | Seabird <> 2

The Cobblestone Collection Skins
AWP | Dragon Lore <> 7
M4A1-S | Knight <> 6
Desert Eagle | Hand Cannon <> 5
CZ75-Auto | Chalice <> 5
MP9 | Dark Age <> 4
P2000 | Chainmail <> 4
USP-S | Royal Blue <> 3
Nova | Green Apple <> 3
MAG-7 | Silver <> 3
Sawed-Off | Rust Coat <> 3
P90 | Storm <> 2
UMP-45 | Indigo <> 2
MAC-10 | Indigo <> 2
SCAR-20 | Storm <> 2
Dual Berettas | Briar <> 2

The Dust Collection Skins
Glock-18 | Brass <> 5
P2000 | Scorpion <> 5
Desert Eagle | Blaze <> 5
Sawed-Off | Copper <> 4
AUG | Copperhead <> 4
AWP | Snake Camo <> 4
AK-47 | Predator <> 3
SCAR-20 | Palm <> 3
M4A4 | Desert Storm <> 3

The Dust 2 Collection Skins
P2000 | Amber Fade <> 5
SG 553 | Damascus Steel <> 4
PP-Bizon | Brass <> 4
M4A1-S | VariCamo <> 4
Sawed-Off | Snake Camo <> 3
AK-47 | Safari Mesh <> 3
Five-SeveN | Orange Peel <> 3
MAC-10 | Palm <> 3
Tec-9 | VariCamo <> 3
G3SG1 | Desert Storm <> 2
P250 | Sand Dune <> 2
SCAR-20 | Sand Mesh <> 2
P90 | Sand Spray <> 2
MP9 | Sand Dashed <> 2
Nova | Predator <> 2

The Gods and Monsters Collection Skins
AWP | Medusa <> 7
M4A4 | Poseidon <> 6
G3SG1 | Chronos <> 5
M4A1-S | Icarus Fell <> 5
UMP-45 | Minotaur's Labyrinth <> 4
MP9 | Pandora's Box <> 4
Tec-9 | Hades <> 3
P2000 | Pathfinder <> 3
AWP | Sun in Leo <> 3
M249 | Shipping Forecast <> 3
MP7 | Asterion <> 2
AUG | Daedalus <> 2
Dual Berettas | Moon in Libra <> 2
Nova | Moon in Libra <> 2

The Inferno Collection Skins
Tec-9 | Brass <> 4
Dual Berettas | Anodized Navy <> 4
M4A4 | Tornado <> 3
P250 | Gunsmoke <> 3
Nova | Walnut <> 2
MAG-7 | Sand Dune <> 2

The Italy Collection Skins
AWP | Pit Viper <> 5
Sawed-Off | Full Stop <> 4
Glock-18 | Candy Apple <> 4
MP7 | Anodized Navy <> 4
XM1014 | CaliCamo <> 3
M4A1-S | Boreal Forest <> 3
UMP-45 | Gunsmoke <> 3
P2000 | Granite Marbleized <> 3
Dual Berettas | Stained <> 3
Nova | Candy Apple <> 3
Tec-9 | Groundwater <> 2
AUG | Contractor <> 2
FAMAS | Colony <> 2
Nova | Sand Dune <> 2
PP-Bizon | Sand Dashed <> 2

The Lake Collection Skins
Dual Berettas | Cobalt Quartz <> 5
USP-S | Night Ops <> 4
P90 | Teardown <> 4
SG 553 | Anodized Navy <> 4
Desert Eagle | Mudder <> 3
AWP | Safari Mesh <> 3
PP-Bizon | Night Ops <> 3
FAMAS | Cyanospatter <> 3
XM1014 | Blue Steel <> 3
P250 | Boreal Forest <> 2
XM1014 | Blue Spruce <> 2
AUG | Storm <> 2
Galil AR | Sage Spray <> 2
SG 553 | Waves Perforated <> 2
G3SG1 | Jungle Dashed <> 2

The Militia Collection Skins
SCAR-20 | Splash Jam <> 6
M4A4 | Modern Hunter <> 5
Nova | Modern Hunter <> 4
PP-Bizon | Modern Hunter <> 4
XM1014 | Blaze Orange <> 4
P250 | Modern Hunter <> 4
Nova | Blaze Orange <> 4
P2000 | Grassland Leaves <> 3
PP-Bizon | Forest Leaves <> 2
MAC-10 | Tornado <> 2
XM1014 | Grassland <> 2

The Mirage Collection Skins
MAG-7 | Bulldozer <> 5
MAC-10 | Amber Fade <> 4
UMP-45 | Blaze <> 4
MP9 | Hot Rod <> 4
Negev | CaliCamo <> 3
SSG 08 | Tropical Storm <> 3
SG 553 | Gator Mesh <> 3
Glock-18 | Groundwater <> 3
MP7 | Orange Peel <> 3
P250 | Bone Mask <> 2
Five-SeveN | Contractor <> 2
AUG | Colony <> 2
G3SG1 | Safari Mesh <> 2
P90 | Scorched <> 2
Galil AR | Hunting Blind <> 2

The Nuke Collection Skins
Tec-9 | Nuclear Threat <> 5
P250 | Nuclear Threat <> 5
M4A4 | Radiation Hazard <> 4
XM1014 | Fallout Warning <> 3
UMP-45 | Fallout Warning <> 3
P90 | Fallout Warning <> 3
PP-Bizon | Irradiated Alert <> 2
Sawed-Off | Irradiated Alert <> 2
MAG-7 | Irradiated Alert <> 2

The Office Collection Skins
MP7 | Whiteout <> 4
P2000 | Silver <> 4
M249 | Blizzard Marbleized <> 3
G3SG1 | Arctic Camo <> 3
Galil AR | Winter Forest <> 3
FAMAS | Contrast Spray <> 2

The Overpass Collection Skins
M4A1-S | Master Piece <> 6
AWP | Pink DDPAT <> 5
USP-S | Road Rash <> 5
CZ75-Auto | Nitro <> 4
XM1014 | VariCamo Blue <> 4
SSG 08 | Detour <> 4
Desert Eagle | Urban DDPAT <> 3
MP7 | Gunsmoke <> 3
Glock-18 | Night <> 3
P2000 | Grassland <> 3
Sawed-Off | Sage Spray <> 2
UMP-45 | Scorched <> 2
M249 | Contrast Spray <> 2
MAG-7 | Storm <> 2
MP9 | Storm <> 2

The Rising Sun Collection Skins
AUG | Akihabara Accept <> 7
AK-47 | Hydroponic <> 6
Five-SeveN | Neon Kimono <> 5
 <> Desert Eagle | Sunset Storm 壱 <> 5
 <> Desert Eagle | Sunset Storm 弐 <> 5
M4A4 | Daybreak <> 5
Galil AR | Aqua Terrace <> 4
MAG-7 | Counter Terrace <> 4
Tec-9 | Terrace <> 4
P250 | Crimson Kimono <> 3
Desert Eagle | Midnight Storm <> 3
PP-Bizon | Bamboo Print <> 2
Sawed-Off | Bamboo Shadow <> 2
Tec-9 | Bamboo Forest <> 2
G3SG1 | Orange Kimono <> 2
P250 | Mint Kimono <> 2

The Safehouse Collection Skins
M4A1-S | Nitro <> 5
SSG 08 | Acid Fade <> 4
Five-SeveN | Silver Quartz <> 4
FAMAS | Teardown <> 4
G3SG1 | VariCamo <> 3
M249 | Gator Mesh <> 3
Galil AR | VariCamo <> 3
USP-S | Forest Leaves <> 3
AUG | Condemned <> 3
MP9 | Orange Peel <> 3
Dual Berettas | Contractor <> 2
SCAR-20 | Contractor <> 2
SSG 08 | Blue Spruce <> 2
Tec-9 | Army Mesh <> 2
MP7 | Army Recon <> 2

The Train Collection Skins
Tec-9 | Red Quartz <> 5
Desert Eagle | Urban Rubble <> 4
Sawed-Off | Amber Fade <> 4
P250 | Metallic DDPAT <> 3
MAG-7 | Metallic DDPAT <> 3
SCAR-20 | Carbon Fiber <> 3
P90 | Ash Wood <> 3
MAC-10 | Candy Apple <> 3
M4A4 | Urban DDPAT <> 3
UMP-45 | Urban DDPAT <> 2
Dual Berettas | Colony <> 2
G3SG1 | Polar Camo <> 2
Five-SeveN | Forest Night <> 2
Nova | Polar Mesh <> 2
PP-Bizon | Urban Dashed <> 2

The Vertigo Collection Skins
Dual Berettas | Demolition <> 5
AK-47 | Black Laminate <> 4
P90 | Glacier Mesh <> 4
PP-Bizon | Carbon Fiber <> 3
MAC-10 | Urban DDPAT <> 2
XM1014 | Urban Perforated <> 2