package Analizer;

import Databases.Database;
import Databases.SkinsDatabase;
import Objects.Info;
import Objects.Post;
import Objects.Skin;
import Utilitaries.Reader;
import Utilitaries.Variables;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataProcessor
{
    private static Reader fileReader;
    private static JSONArray posts;
    private static int idx;

    public static void setVariables()
    {
        fileReader = new Reader(Variables.CONFIG_FILE);

        Variables.PASS = fileReader.getXLine(2).split(": ")[1];
        Variables.USER = fileReader.getXLine(1).split(": ")[1];
        Variables.URL =fileReader.getXLine(4).split(": ")[1] + fileReader.getXLine(3).split(": ")[1];
        Variables.KEY_VALUE = Double.parseDouble(fileReader.getXLine(5).split(": ")[1]);
        Variables.JSON_FILE = new File(fileReader.getXLine(6).split(": ")[1]);
        Variables.WeaponsJSONFile = new File(fileReader.getXLine(7).split(": ")[1]);
    }

    public static void processJSONFile()
    {
        JSONParser parser = new JSONParser();
        fileReader = new Reader(Variables.JSON_FILE);

        try
        {
            Object object = parser.parse(new FileReader(Variables.JSON_FILE));
            JSONObject jsonObject = (JSONObject) object;
            jsonObject = (JSONObject) jsonObject.get("data");
            posts =(JSONArray) jsonObject.get("children");
        }
        catch (ParseException e) {
            System.out.println("Error during JSON parsing process");
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found\npath: " + Variables.JSON_FILE);
        }
        catch (IOException e)
        {
            System.out.println("Some sort of error reading the json file");
        }
    }

    public static Post getNextPost(Database skinData, Database weaponData)
    {
        boolean valid=false;
        Post post = new Post();
        JSONObject currentPost;

        while(!valid)
        {
            if(posts.size()==0)
                return null;
            currentPost = (JSONObject) posts.get(0);
            currentPost = (JSONObject)currentPost.get("data");

            valid = isAValidPost(currentPost) && getComposedPost(currentPost, post, skinData, weaponData);

            posts.remove(0);
        }

        return post;
    }

    private static boolean isAValidPost(JSONObject post)
    {
        String title = (post.get("title")).toString().toLowerCase();

        if(title.charAt(1)!='h') return false;
        title=title.substring(3);
        String [] HW = title.split("\\[w\\]");

        Pattern pattern = Pattern.compile("(\\d*(\\d+|\\s)k(e|\\s|$|\\W))");

        Matcher matcher = pattern.matcher(HW[0]);
        if (matcher.find()) return false;

        matcher = pattern.matcher(HW[1]);
        return matcher.find();
    }

    private static boolean getComposedPost(JSONObject currentPost, Post post, Database skinData, Database weaponData)
    {
        String text = (currentPost.get("selftext")).toString().toLowerCase();

        return getPrice(text, post) &&
                getTradeLink(text, post) &&
                getPostAndProfile(currentPost, post) &&
                getSkinInfo(post, currentPost, skinData,weaponData);

    }

    private static boolean getSkinInfo(Post post, JSONObject currentPost , Database skinData, Database weaponData)
    {
        String text = (currentPost.get("selftext")).toString().toLowerCase();
        String [] processed = getCloserWords(text, Integer.toString(post.price));
        int nrAttempts =2;

        if (processed!=null)
        {
            if (processed.length < 2)
            {
                processed = (currentPost.get("title")).toString().toLowerCase().split(" ");
                nrAttempts--;
            }

            Info gun, skin;
            for(int x=0;x<nrAttempts;x++)
            {
                if ((gun=findGun(processed,weaponData))!=null)
                {
                    processed = Arrays.copyOfRange(processed, idx-2 < 0 ? 0 : idx-2, idx + 5 > processed.length ? processed.length : idx + 5);

                    if ((skin=findSkin(processed, skinData))!=null)
                    {
                        post.skin = new Skin(gun.getName() + " | " + skin.getName());
                        post = (findWear(processed,post));
                        post = findStatTrak(processed,post);
                        post = findSkinPrice(processed,post);
                        if(post.skin.getPrice()!=0)
                            return true;
                    }
                }

                if(nrAttempts==2 && x==0)
                    processed = (currentPost.get("title")).toString().toLowerCase().split(" ");
            }
        }
        return false;
    }

    private static Post findSkinPrice(String[] processed, Post post)
    {
        post.skin.setPrice(SkinsDatabase.GetPrice(post.skin.getName(), post.skin.getWear()));
        return post;
    }

    private static Post findStatTrak(String[] processed, Post post)
    {
        String [] valid = {"stattrak","stat trak","stat track","stattrack","st"};
        for (String s : processed)
        {
            for (String s1 : valid)
            {
                if(s.equals(s1))
                {    post.skin.setStatTrak(true);
                    return post;
                }
            }
        }

        return post;
    }

    private static Post findWear(String[] processed, Post post)
    {
        for (String s : processed)
        {
            s=s.toLowerCase();
            for (String[] validWear : Variables.validWears)
            {
                if(s.charAt(0) == validWear[0].charAt(0))
                    for (String s1 : validWear)
                    {
                        if(s1.toLowerCase().equals(s))
                            post.skin.setWear(validWear[1] + " " + validWear[2]);
                    }
            }
        }

        Pattern pattern = Pattern.compile("\\d+\\.\\d+");
        for (String s : processed)
        {
            Matcher matcher = pattern.matcher(s);
            if (matcher.find())
            {
                post.skin.setFloatValue(Double.parseDouble(matcher.group()));
                post.skin.setWear(getWearByFloat(matcher.group()));
                break;
            }
        }
        return post;
    }

    private static String getWearByFloat(String toConvert)
    {
        double [] floatValue = {0.06,0.15,0.37,0.44,1};
        String [] wear = {"Factory new", "Minimal Wear","Field-Tested","Well-Worn","Battle-Scarred"};
        double converted = Double.parseDouble(toConvert);

        for (int i=0; i<floatValue.length; i++)
        {
            if(converted<floatValue[i])
                return wear[i];
        }
        return "undefined";
    }

    private static Info findSkin(String[] processed, Database skinData)
    {
        for (int i = idx; i < processed.length; i++)
        {
            Info skin = skinData.getIfExistant(processed);
            if (skin != null)
                return skin;
        }

        return null;
    }

    private static Info findGun(String[] processed, Database weaponData)
    {
        for (int i = 0; i < processed.length; i++)
        {
            Info gun = weaponData.getIfExistant(processed[i]);
            if (gun != null)
            {
                idx = i;
                return gun;
            }
        }

        return null;
    }

    private static String [] getCloserWords(String text, String match)
    {
        String [] raw = text.split(" ");
        int found=-1;

        for (int i = 0; i < raw.length; i++)
        {
            if(raw[i].length()>= match.length() && raw[i].substring(0,match.length()).equals(match))
            {
                found = i;
                break;
            }
        }

        if (found!=-1)
            return (found-10) >= 0 ? Arrays.copyOfRange(raw,found-10,found) : Arrays.copyOfRange(raw,0,found);

        return null;
    }

    private static boolean getPostAndProfile(JSONObject currentPost, Post post)
    {
        try
        {
            post.poster = new URL(currentPost.get("author_flair_text").toString());
            post.post = new URL(currentPost.get("url").toString());
        }
        catch (MalformedURLException e)
        {
            return false;
        }

        return true;
    }

    private static boolean getTradeLink(String text, Post post)
    {
        Pattern pattern = Pattern.compile("https://steamcommunity.com/tradeoffer/new/\\?\\S+");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
            try
            {
                post.tradeLink = new URL(matcher.group());
                return true;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        return false;
    }

    private static boolean getPrice(String text, Post post)
    {
        Pattern pattern = Pattern.compile("b/?o\\W*\\d+\\s*k(e|\\s|$)");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
        {
            String match = matcher.group();
            Pattern testP = Pattern.compile("\\d+");
            Matcher testM = testP.matcher(match);

            if (testM.find())
            {
                post.price = Integer.parseInt(testM.group());
                return true;
            }
        }
        return false;
    }
}
