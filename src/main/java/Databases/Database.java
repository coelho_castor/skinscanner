package Databases;

import Objects.Info;
import Utilitaries.Reader;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Filipe G. Coelho.
 */

public class Database
{
    private ArrayList<ArrayList<Info>> data = new ArrayList<>();


    public Database()
    {
        dataInit();
    }

    private void dataInit()
    {
        int ALPHABET_LENGTH = 26;
        for (int i = 0; i < ALPHABET_LENGTH; i++)
        {
            data.add(new ArrayList<>());
        }
    }

    public void fillDatabase(File dataFile)
    {
        Reader reader = new Reader(dataFile);

        String line;
        while((line=reader.getNextLine())!=null)
        {
            this.data.get(line.toLowerCase().charAt(0)-'a').add(new Info(line));
        }

    }

    public Info getIfExistant(String candidate)
    {
        if(candidate!= null && candidate.length()>0 && Character.isAlphabetic(candidate.charAt(0)))
        {
            ArrayList<Info> toAnalise = data.get(candidate.charAt(0) - 'a');
            for (Info info : toAnalise) {
                if (info.similar(candidate))
                    return info;
            }
        }
        return null;
    }

    public Info getIfExistant(String [] candidate)
    {
        for (String aCandidate : candidate)
        {
            if (Character.isAlphabetic(aCandidate.toLowerCase().charAt(0)))
            {
                ArrayList<Info> toAnalise = data.get(aCandidate.toLowerCase().charAt(0) - 'a');
                for (Info info : toAnalise)
                {
                    if (info.similar(aCandidate.toLowerCase()))
                        return info;
                }
            }
        }

        for (int i = 0; i < candidate.length-1; i++)
        {
            if(Character.isAlphabetic(candidate[i].charAt(0)) && Character.isAlphabetic(candidate[i+1].charAt(0)))
            {
                ArrayList<Info> toAnalise = data.get(candidate[i].charAt(0) - 'a');
                for (Info info : toAnalise) {
                    if (info.similar(candidate[i] + " " + candidate[i+1]))
                        return info;
                }
            }
        }

        return null;
    }
}
