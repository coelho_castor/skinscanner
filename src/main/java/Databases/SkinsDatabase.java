package Databases;

import Objects.CastroSkin;
import Utilitaries.Variables;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.lang.Float;
import java.lang.Boolean;
import java.util.regex.Pattern;

public class SkinsDatabase
{
	public static Boolean IsImported = false;
	public static List<CastroSkin> SkinsList = new ArrayList<CastroSkin>();

	public static void ImportSkinsDatabase()
	{
		JSONParser parser = new JSONParser();

		JSONArray JSONArr = new JSONArray();

		try
		{
			JSONArr = (JSONArray) parser.parse(new FileReader(Variables.WeaponsJSONFile));
		}
		catch (ParseException e)
		{
            System.err.println("Error parsing weapons.json");
        }
		catch (FileNotFoundException e)
		{
            System.err.println("File not found\npath: "+Variables.WeaponsJSONFile);
        }
        catch (IOException e)
        {
            System.err.println("Some sort of error reading "+Variables.WeaponsJSONFile);
        }

		List<CastroSkin>TempSkinsList = new ArrayList<CastroSkin>();

		for(Object o : JSONArr)
		{
			JSONObject SkinJSONObj = (JSONObject) o;

			CastroSkin SkinObj = new CastroSkin();

			SkinObj.Name = (String) SkinJSONObj.get("Name");

			Object Type = SkinJSONObj.get("Type");
			if(Type != null)
			{
				SkinObj.Type = CastroSkin.SkinTypesEnum.valueOf((String) Type);
			}

			Object SteamAnalystId = SkinJSONObj.get("SteamAnalystId");
			if(SteamAnalystId != null)
			{
				SkinObj.SteamAnalystId = (String) SkinJSONObj.get("SteamAnalystId");
			}

			Object Has_StatTrak = SkinJSONObj.get("Has_StatTrak");
			if(Has_StatTrak != null)
			{
				long Has_StatTrak_Long = (long) Has_StatTrak;

				if(Has_StatTrak_Long == 0)
				{
					SkinObj.Has_StatTrak = false;
				}
				else{
					SkinObj.Has_StatTrak = true;
				}
			}

			Object LastPriceUpdateTimestamp = SkinJSONObj.get("LastPriceUpdateTimestamp");
			if(LastPriceUpdateTimestamp == null)
			{
				SkinObj.LastPriceUpdateTimestamp = 0;
			}
			else
			{
				SkinObj.LastPriceUpdateTimestamp = (long) LastPriceUpdateTimestamp;
			}



			Object BattleScarredPrice = SkinJSONObj.get("BattleScarredPrice");
			if(BattleScarredPrice != null)
			{
				SkinObj.BattleScarredPrice = ((Number) BattleScarredPrice).doubleValue();
			}

			Object WellWornPrice = SkinJSONObj.get("WellWornPrice");
			if(WellWornPrice != null)
			{
				SkinObj.WellWornPrice = ((Number) WellWornPrice).doubleValue();
			}

			Object FieldTestedPrice = SkinJSONObj.get("FieldTestedPrice");
			if(FieldTestedPrice != null)
			{
				SkinObj.FieldTestedPrice = ((Number) FieldTestedPrice).doubleValue();
			}

			Object MinimalWearPrice = SkinJSONObj.get("MinimalWearPrice");
			if(MinimalWearPrice != null)
			{
				SkinObj.MinimalWearPrice = ((Number) MinimalWearPrice).doubleValue();
			}

			Object FactoryNewPrice = SkinJSONObj.get("FactoryNewPrice");
			if(FactoryNewPrice != null)
			{
				SkinObj.FactoryNewPrice = ((Number) FactoryNewPrice).doubleValue();
			}

			TempSkinsList.add(SkinObj);
		}

		SkinsList = TempSkinsList;
		IsImported = true;
		
		//System.out.println("Number of Skins: "+SkinsList.size());
	}

	@SuppressWarnings("unchecked")
	public static void ExportSkinsDatabase()
	{
		if(!IsImported)
		{
			return;
		}

		JSONArray JSONArr = new JSONArray();

		for (int i = 0; i < SkinsList.size(); i++)
		{
			CastroSkin SkinObj = SkinsList.get(i);

			JSONObject JSONObj = new JSONObject();

			JSONObj.put("Name", SkinObj.Name);

			if(SkinObj.Type != null){
				JSONObj.put("Type", SkinObj.Type.name());
			}

			JSONObj.put("SteamAnalystId", SkinObj.SteamAnalystId);

			if(SkinObj.Has_StatTrak != null){
				if(SkinObj.Has_StatTrak)
				{
					JSONObj.put("Has_StatTrak", 1);
				}
				else{
					JSONObj.put("Has_StatTrak", 0);
				}
			}
			
			JSONObj.put("LastPriceUpdateTimestamp", SkinObj.LastPriceUpdateTimestamp);

			JSONObj.put("BattleScarredPrice", SkinObj.BattleScarredPrice);
			JSONObj.put("WellWornPrice", SkinObj.WellWornPrice);
			JSONObj.put("FieldTestedPrice", SkinObj.FieldTestedPrice);
			JSONObj.put("MinimalWearPrice", SkinObj.MinimalWearPrice);
			JSONObj.put("FactoryNewPrice", SkinObj.FactoryNewPrice);

			JSONArr.add(JSONObj);
		}

		try
		{
			FileWriter JSONFile = new FileWriter(Variables.WeaponsJSONFile);
			JSONFile.write(JSONArr.toJSONString());
			JSONFile.flush();
			JSONFile.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void AddSkinsFromTXT()
	{
		if(!IsImported)
		{
			ImportSkinsDatabase();
		}

		Boolean AtLeastOneSkinAdded = false;
		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader("src\\main\\resources\\weapons.txt");
			
			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String line = null;

			while ((line = bufferedReader.readLine()) != null)
			{
				String[] SkinDetailsArray = line.split(" <> ");
				if (SkinDetailsArray.length == 2)
				{
					GetOrAddSkinByName(SkinDetailsArray[0], CastroSkin.SkinTypesEnum.Normal);
					/*if(GetOrAddSkinByName(SkinDetailsArray[0]) == null)
					{
						CastroSkin SkinObj = new CastroSkin();
						SkinObj.Name = SkinDetailsArray[0];

						SkinsList.add(SkinObj);
						AtLeastOneSkinAdded = true;
					}*/
				}
			}
		}
		catch(FileNotFoundException ex)
		{
			System.out.println("Unable to open weapons.txt");
        }
        catch(IOException ex)
		{
			System.out.println("Error reading file weapons.txt");
        }

		if(AtLeastOneSkinAdded)
		{
			ExportSkinsDatabase();
		}
	}

	public static CastroSkin GetOrAddSkinByName(String SkinName, CastroSkin.SkinTypesEnum SkinType)
	{
		if(!IsImported)
		{
			ImportSkinsDatabase();
		}

		for (int i = 0; i < SkinsList.size(); i++)
		{
			CastroSkin TempSkinObj = SkinsList.get(i);
			if(TempSkinObj.Type == SkinType && SkinName.equals(TempSkinObj.Name))
			{
				return TempSkinObj;
			}
		}

		CastroSkin SkinObj = new CastroSkin();
		SkinObj.Name = SkinName;
		SkinObj.Type = SkinType;
		
		SkinsList.add(SkinObj);

		ExportSkinsDatabase();

		return SkinObj;
	}

	public static double GetPrice(String SkinName, String SkinWear)
	{
		return GetPrice(SkinName, SkinWear, CastroSkin.SkinTypesEnum.Normal);
	}

	public static double GetPrice(String SkinName, String SkinWear, String SkinType)
	{
		double Price = 0;

		switch (SkinType)
		{
			case "Normal":
				Price = GetPrice(SkinName, SkinWear, CastroSkin.SkinTypesEnum.Normal);
				break;
			case "StatTrak":
				Price = GetPrice(SkinName, SkinWear, CastroSkin.SkinTypesEnum.StatTrak);
				break;
			default:
				System.out.println("Invalid SkinType: "+SkinType);
				break;
		}

		return Price;
	}

	public static double GetPrice(String SkinName, String SkinWear, CastroSkin.SkinTypesEnum SkinType)
	{
		//System.out.println("SkinName: "+SkinName);
		//System.out.println("SkinWear: "+SkinWear);

		if(!IsImported)
		{
			ImportSkinsDatabase();
		}

		CastroSkin SkinObj = GetOrAddSkinByName(SkinName, SkinType);

		System.out.println(SkinObj.Has_StatTrak);

		if(SkinObj.LastPriceUpdateTimestamp + 1800 < new java.util.Date().getTime() / 1000)
		{
			RefreshPrice(SkinObj);
		}

		double Price = 0;

		switch (SkinWear)
		{
			case "Battle-Scarred":
				Price = SkinObj.BattleScarredPrice;
				break;
			case "Well-Worn":
				Price = SkinObj.WellWornPrice;
				break;
			case "Field-Tested":
				Price = SkinObj.FieldTestedPrice;
				break;
			case "Minimal Wear":
				Price = SkinObj.MinimalWearPrice;
				break;
			case "Factory New":
				Price = SkinObj.FactoryNewPrice;
				break;
			default:
				System.out.println("Invalid Skin Wear: "+SkinWear);
				break;
		}

		return Price;
	}

	public static void RefreshSteamAnalystId(CastroSkin SkinObj)
	{
		if(!IsImported)
		{
			ImportSkinsDatabase();
		}

		String SteamAnalystId = null;

		try
		{
			URL url = new URL(
				"http://csgo.steamanalyst.com/autocomplete.php?query="+
				URLEncoder.encode(SkinObj.Name.replace("|", ""), "UTF-8")
			);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			//Para o steamanalyst nao mandar 403 error
			con.setRequestProperty("referer", "http://csgo.steamanalyst.com/");
			con.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36");

			BufferedReader SteamAnalystResponse = null;

			Boolean HTTPSuccess = true;
			try
			{
				SteamAnalystResponse = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			catch(IOException ex)
			{
				HTTPSuccess = false;
			}

			if(HTTPSuccess)
			{
				JSONParser parser = new JSONParser();
				JSONObject JSONObj = null;

				try
				{
					JSONObj = (JSONObject) parser.parse(SteamAnalystResponse);
				}
				catch (ParseException e)
				{
					System.out.println("Error during JSON parsing process");
				}
				catch (FileNotFoundException e)
				{
					System.out.println("File not found");
				}
				catch (IOException e)
				{
					System.out.println("Some sort of error reading the json file");
				}

				if(JSONObj != null)
				{
					JSONArray Suggestions = (JSONArray) JSONObj.get("suggestions");

					for(Object o : Suggestions)
					{
						JSONObject SuggestionObj = (JSONObject) o;

						SkinObj.SteamAnalystId = (String) SuggestionObj.get("data");

						ExportSkinsDatabase();

						break;
					}
				}
			}
			else if(con.getResponseCode() == 403)
			{
				System.out.println("steamanalyst.com returned 403 error for name search");
			}
			else
			{
				System.out.println("HTTP Error in GetSteamAnalystIdFromName. Code: "+con.getResponseCode());
			}
		}
		catch(IOException ex)
		{
			System.out.println("Error requesting steamanalyst.com");
		}
	}

    public static void RefreshPrice(CastroSkin SkinObj)
    {
		if(!IsImported)
		{
			ImportSkinsDatabase();
		}

		String SteamAnalystId = null;

		if(SkinObj.Type == CastroSkin.SkinTypesEnum.StatTrak){
			if(SkinObj.SteamAnalystId == null)
			{
				CastroSkin NormalSkinObj = GetOrAddSkinByName(SkinObj.Name, CastroSkin.SkinTypesEnum.Normal);
				if(NormalSkinObj.Has_StatTrak == null){
					RefreshPrice(NormalSkinObj);
				}

				if(!NormalSkinObj.Has_StatTrak){
					return;
				}
			}

			SteamAnalystId = SkinObj.SteamAnalystId;
		}
		else{
			if(SkinObj.SteamAnalystId == null)
			{
				RefreshSteamAnalystId(SkinObj);
			}

			SteamAnalystId = SkinObj.SteamAnalystId;
		}

		try
		{
			URL url = new URL(
				"http://csgo.steamanalyst.com/id/"+
				SteamAnalystId
			);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			//Para o steamanalyst nao mandar 403 error//Ainda nao verifiquei se aqui e mesmo necessario
			con.setRequestProperty("referer", "http://csgo.steamanalyst.com/");
			con.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36");

			String PricesResponse = null;

			Boolean HTTPSuccess = true;
			try
			{
				BufferedReader PricesResponseReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

				StringBuilder PricesResponseStringBuilder = new StringBuilder();
				String PricesResponseLine;
				while((PricesResponseLine = PricesResponseReader.readLine()) != null)
				{
					PricesResponseStringBuilder.append(PricesResponseLine);
					PricesResponseStringBuilder.append("\n");
				}
				PricesResponseReader.close();
				PricesResponse = PricesResponseStringBuilder.toString();
			}
			catch(IOException ex)
			{
				HTTPSuccess = false;
			}

			if(HTTPSuccess)
			{
				if(SkinObj.Type == CastroSkin.SkinTypesEnum.Normal){
					String[] StatTrakIdParts = PricesResponse.split("<div id=\"stattrakLink\">");
					if(StatTrakIdParts.length == 2)
					{
						String StatTrak_SteamAnalystId = StatTrakIdParts[1].split("/id/")[1].split("\"")[0];

						CastroSkin StatTrakSkinObj = GetOrAddSkinByName(SkinObj.Name, CastroSkin.SkinTypesEnum.StatTrak);
						StatTrakSkinObj.SteamAnalystId = StatTrak_SteamAnalystId;

						SkinObj.Has_StatTrak = true;
					}
					else{
						SkinObj.Has_StatTrak = false;
					}
				}

				String[] WeaponConditionClasses = {"bs", "ww", "ft", "mw", "fn"};
				for (int i = 0; i < 5; i++)
				{
					String[] PricesResponseParts = PricesResponse.split("<li class=\""+WeaponConditionClasses[i]+"\">");

					if(PricesResponseParts.length == 2)
					{
						String PriceSpan = PricesResponseParts[1].split("</span>")[0].split("<span>")[1];

						//System.out.println(PriceSpan);

						double Price = 0;

						try
						{
							Price = Float.parseFloat(PriceSpan.split(Pattern.quote("$"))[1].split(" ")[0]);
						}
						catch(ArrayIndexOutOfBoundsException ex)
						{
						}

						switch (i)
						{
							case 0:
								SkinObj.BattleScarredPrice = Price;
								break;
							case 1:
								SkinObj.WellWornPrice = Price;
								break;
							case 2:
								SkinObj.FieldTestedPrice = Price;
								break;
							case 3:
								SkinObj.MinimalWearPrice = Price;
								break;
							case 4:
								SkinObj.FactoryNewPrice = Price;
								break;
						}
					}
				}

				SkinObj.LastPriceUpdateTimestamp = new java.util.Date().getTime() / 1000;

				ExportSkinsDatabase();
			}
			else if(con.getResponseCode() == 403)
			{
				System.out.println("steamanalyst.com returned 403 error for the price request");
			}
			else
			{
				System.out.println("HTTP Error in RefreshPrice. Code: "+con.getResponseCode());
			}
		}
		catch(IOException ex)
		{
			System.out.println("Error requesting steamanalyst.com");
        }
    }
}
