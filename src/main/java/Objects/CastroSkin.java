package Objects;

import java.util.List;
import java.lang.Boolean;

public class CastroSkin
{
	public enum SkinTypesEnum {Normal, StatTrak};

    public String Name;
	public SkinTypesEnum Type;

	public String SteamAnalystId;
	public Boolean Has_StatTrak;

	public long LastPriceUpdateTimestamp;

	public double BattleScarredPrice;
	public double WellWornPrice;
	public double FieldTestedPrice;
	public double MinimalWearPrice;
	public double FactoryNewPrice;
}