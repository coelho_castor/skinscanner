package Objects;

import java.net.URL;

public class Post
{
    public URL poster;
    public URL tradeLink;
    public URL post;
    public Skin skin;
    public int price;

    public String toString()
    {
        return "Poster: " + poster.toString() +
               "\n\tTradelink: " + tradeLink.toString() +
               "\n\tPost: " + post.toString() +
               "\n\tSkin:\n\t\t" + skin.toString() +
               "\n\tPoster price: " + price + "keys\n\n";
    }
}
