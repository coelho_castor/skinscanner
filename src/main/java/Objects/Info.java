package Objects;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Filipe G. Coelho.
 */
public class Info
{
    private String name;
    private ArrayList<String> tags;

    public Info(String info)
    {
        tags=new ArrayList<>();
        setVariables(info);
    }

    public String getName()
    {
        return name;
    }

    private void setVariables(String info)
    {
        String [] splitted = info.split(";");


        if(splitted.length>1)
        {
            splitted[1]=splitted[1].toLowerCase();
            String[] tags = splitted[1].split("::");
            Collections.addAll(this.tags, tags);
        }

        this.name = splitted[0];
    }

    public boolean similar (String candidate)
    {
        return candidate.toLowerCase().equals(name.toLowerCase()) || tags.contains(candidate);
    }
}
