package Objects;

public class Skin
{
	private String name;
    private String wear="undefined";
    private double floatValue=-1;
    private boolean statTrak=false;
	private double price=-1;

	public Skin(String name, int price)
	{
		this.name = name;
		this.price = price;
	}

	public Skin(String name , String wear)
	{
		this.name = name;
        this.wear = wear;
	}

    public Skin(String name)
    {
        this.name = name;
    }

    public String toString()
	{
        return "Name: " + name +
                "\n\t\tWear: " + wear +
                "\n\t\tStatTrak: " + Boolean.toString(statTrak) +
                "\n\t\tMarket price: " + (price == -1 ? "undefined" : Double.toString(price)) +
                "\n\t\tFloat value: " + (floatValue == -1 ? "undefined" : Double.toString(floatValue));
	}


    public String getName() {
        return name;
    }

    public String getWear() {
        return wear;
    }

    public Double getPrice() {
        return price;
    }

    public void setWear(String wear) {
        this.wear = wear;
    }

    public void setFloatValue(double floatValue) {
        this.floatValue = floatValue;
    }

    public void setStatTrak(boolean statTrak)
    {
        this.statTrak = statTrak;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }
}