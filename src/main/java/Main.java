import Analizer.DataProcessor;
import Databases.Database;
import Databases.SkinsDatabase;
import Objects.Post;
import Utilitaries.DataRequest;
import Utilitaries.Variables;

/**
 * Created by Filipe G. Coelho.
 */
public class Main
{
    private static Database model_DataBase,skin_DataBase;

    public static void main(String[]args)
    {
        DataProcessor.setVariables();       //Loading variable from the configuration file
        initDatabases();                    //Fill the existent Databases with skin info

        DataRequest.requestJSONFile();      //Request the web info in JSON format, and Writing the info on a file
        DataProcessor.processJSONFile();    //Convert JSON file into a POST array, POST represents a website post

        Post post;
        while ((post = DataProcessor.getNextPost(skin_DataBase,model_DataBase)) != null)
        {
            System.out.println(post.toString());
        }
    }

    private static void initDatabases()
    {
        model_DataBase = new Database();
        model_DataBase.fillDatabase(Variables.MODEL_DATA);

        skin_DataBase = new Database();
        skin_DataBase.fillDatabase(Variables.SKIN_DATA);
    }
}
