package Utilitaries;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class DataRequest
{
    private static BufferedReader netReader;
    private static BufferedWriter fileWriter;

    public static void requestJSONFile()
    {
        netReaderInit();
        fileWriterInit();

        requestAndWrite();
    }

    private static void requestAndWrite()
    {
        String line;
        try
        {
            while((line=netReader.readLine())!=null)
            {
                fileWriter.write(line);
            }

            netReader.close();
            fileWriter.close();

        }
        catch (IOException e)
        {
            System.out.println("Problem with getting or processing info from: " + Variables.URL);
        }
    }

    private static void fileWriterInit()
    {
        try
        {
            fileWriter = new BufferedWriter(new FileWriter(Variables.JSON_FILE));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void netReaderInit()
    {
        try
        {
            URL url = new URL(Variables.URL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent","User-Agent: java:RedditScanner:v1.0 (by /u/" + Variables.USER + ")");
            netReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

}

