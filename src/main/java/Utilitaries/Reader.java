package Utilitaries;
import java.io.*;

public class Reader
{
    private BufferedReader reader;
    private File file;

    public Reader(File file)
    {
        this.file=file;
        startReader();
    }

    private void startReader()
    {
        try
        {
            reader = new BufferedReader(new FileReader(file));
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File path not found...\npath: "+ file.getAbsolutePath() + "\n\nexiting...");
            System.exit(0);
        }
    }

    public String getXLine(int x)
    {
        try
        {
            startReader();
            for(int i=1;i<x; i++)
            {
                reader.readLine();
            }

            return reader.readLine();
        }
        catch(Exception e)
        {
            System.out.println("Error while reading from a text file");
        }

        return null;
    }

    public String getNextLine()
    {
        try
        {
            return reader.readLine();
        }
        catch (IOException e)
        {
            System.out.println("Error while reading next line.\nProbably reader not initialized");
        }

        return null;
    }
}
