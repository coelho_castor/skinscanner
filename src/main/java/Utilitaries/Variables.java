package Utilitaries;

import java.io.File;

public class Variables
{
    public static String URL = null;
    public static String USER = null;
    public static String PASS = null;
    public static double KEY_VALUE;
    public static File CONFIG_FILE = new File("src\\main\\resources\\files\\config.txt");
    public static File JSON_FILE;
	public static File WeaponsJSONFile = new File("src\\main\\resources\\files\\skins_database.json");
    public static File MODEL_DATA = new File("src\\main\\resources\\files\\weapons_name.txt");
    public static File SKIN_DATA = new File("src\\main\\resources\\files\\skins_name.txt");
	public static String [][] validWears = {{"Battle-Scared","battle", "scared","bs"},{"Well-Worn","well","worn","ww"},{"Field-Tested","field","tested","ft"},{"Minimal Wear","minimal","wear","mw"},{"Factory New","factory","new","fn"}};
}
